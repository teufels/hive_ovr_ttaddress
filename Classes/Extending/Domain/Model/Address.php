<?php

namespace HIVE\HiveOvrTtaddress\Extending\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

class Address extends \FriendsOfTYPO3\TtAddress\Domain\Model\Address
{

    /** @var string */
    protected $department = '';

    /** @var string */
    protected $qualification = '';

    /** @var string */
    protected $vita = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $awards;

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param string $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return string
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    /**
     * @param string $qualification
     */
    public function setQualification($qualification)
    {
        $this->qualification = $qualification;
    }

    /**
     * @return string
     */
    public function getVita()
    {
        return $this->vita;
    }

    /**
     * @param string $vita
     */
    public function setVita($vita)
    {
        $this->vita = $vita;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    public function getAwards(): ?ObjectStorage
    {
        return $this->awards;
    }

    /**
     * @param ObjectStorage<FileReference> $awards
     */
    public function setAwards(ObjectStorage $awards): void
    {
        $this->awards = $awards;
    }
}
# Extend table structure for table 'tt_address'
CREATE TABLE tt_address (
    department varchar(255) DEFAULT '' NOT NULL,
    qualification text,
    vita text,
    awards int(11) unsigned NOT NULL DEFAULT '0',
);
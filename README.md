![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__ovr__ttaddress-blue.svg)
![version](https://img.shields.io/badge/version-2.0.*-yellow.svg?style=flat-square)

hive_ovr_ttaddress is an extension that extends the tt_address module of the typo3 CMS by the following fields
- department (String)
- qualification (Text)
- vita (RTE)
- awards (Image)

and add/changes BE Palette to
- Organisation: company, department, position
- Qualification : qualification
- Vita: vita
- Awards: awards

### This version supports TYPO3
![TYPO3Version](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req beewilly/hive_ovr_ttaddress`

### Requirements
- `friendsoftypo3/tt-address`
- `evoweb/extender`

### How to use
- Install with composer
- Override Layout/Template/Partial in Theme-EXT to output the additional fields in the frontend

### Changelog
- 2.0.0 adjustments for breaking changed of evoweb/extender 10.0 
- 1.0.0 init
- [...see bitbucket Commits]
<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "hive_ovr_ttaddress"
 *
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['hive_ovr_ttaddress'] = [
    'title' => 'HIVE>Override tt_address',
    'description' => 'Override friendsoftypo3/tt-address EXT to add additional fields',
    'category' => 'plugin',
    'author' => 'teufels GmbH',
    'author_email' => 'digital@teudels.com',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '2.0.0',
    'constraints' => [
        'depends' => [
            'extender' => '10.1.0-0.0.0',
            'tt-address' => '8.0.1-0.0.0',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];

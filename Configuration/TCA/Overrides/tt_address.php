<?php
defined('TYPO3_MODE') or die();

$columns = [
    'department' => [
        'exclude' => true,
        'label' => 'LLL:EXT:hive_ovr_ttaddress/Resources/Private/Language/locallang_db.xlf:hive_ovr_ttaddress.department',
        'config' => [
            'type' => 'input',
            'eval' => 'trim',
            'size' => 20,
            'max' => 255,
            'behaviour' => [
                'allowLanguageSynchronization' => true,
            ],
        ]
    ],
    'qualification' => [
        'exclude' => false,
        'label' => 'LLL:EXT:hive_ovr_ttaddress/Resources/Private/Language/locallang_db.xlf:hive_ovr_ttaddress.qualification',
        'config' => [
            'type' => 'text',
            'enableRichtext' => true,
            'richtextConfiguration' => 'default',
            'fieldControl' => [
                'fullScreenRichtext' => [
                    'disabled' => false,
                ],
            ],
            'cols' => 40,
            'rows' => 15,
            'eval' => 'trim'
        ]
    ],
    'awards' => [
        'exclude' => true,
        'label' => 'LLL:EXT:hive_ovr_ttaddress/Resources/Private/Language/locallang_db.xlf:hive_ovr_ttaddress.awards',
        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
            'awards',
            [
                'maxitems' => 6,
                'minitems' => 0,
                'appearance' => [
                    'collapseAll' => true,
                    'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
                'overrideChildTca' => [
                    'types' => [
                        '0' => [
                            'showitem' => '
                                    --palette--;' . 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                                    --palette--;' . 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                                    --palette--;' . 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                                    --palette--;' . 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                                    --palette--;' . 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                                    --palette--;' . 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                                    --palette--;;filePalette'
                        ],
                    ],
                ],
            ],
            $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
        ),
    ],
    'vita' => [
        'exclude' => false,
        'label' => 'LLL:EXT:hive_ovr_ttaddress/Resources/Private/Language/locallang_db.xlf:hive_ovr_ttaddress.vita',
        'config' => [
            'type' => 'text',
            'enableRichtext' => true,
            'richtextConfiguration' => 'default',
            'fieldControl' => [
                'fullScreenRichtext' => [
                    'disabled' => false,
                ],
            ],
            'cols' => 40,
            'rows' => 15,
            'eval' => 'trim'
        ]
    ],
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_address', $columns);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
    'tt_address',
    'organization',
    'department',
    'after:company'
);

$GLOBALS['TCA']['tt_address']['palettes']['palette_qualification'] = [
    'canNotCollapse' => true,
    'showitem' => 'qualification, --linebreak--,awards'
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_address',
    ',--div--;LLL:EXT:hive_ovr_ttaddress/Resources/Private/Language/locallang_db.xlf:hive_ovr_ttaddress.tab.qualification,--palette--;;palette_qualification',
    '',
    ''
);

$GLOBALS['TCA']['tt_address']['palettes']['palette_vita'] = [
    'canNotCollapse' => true,
    'showitem' => 'vita'
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_address',
    ',--div--;LLL:EXT:hive_ovr_ttaddress/Resources/Private/Language/locallang_db.xlf:hive_ovr_ttaddress.tab.vita,--palette--;;palette_vita',
    '',
    ''
);